import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  const chat = document.createElement('div');
  chat.setAttribute('class', 'chat');
  document.body.append(chat);
  console.log(chat);
});
